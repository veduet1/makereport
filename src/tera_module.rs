use std::collections::{HashMap, LinkedList};
use tera::Map;
use tera::{Value, from_value, try_get_value, Filter};
use tracing::debug;
use crate::{optional_arg, required_arg};

/// Search selectors an return their value in a list
///
/// The selector's format is the last one:
/// `{ selector_name }`
///
/// # Arguments
///
///  * s : The string to find selectors
///
/// # Return
///
/// Return None when the selector is ill-formed or a linked list with found
/// selectors.
fn seek_selector(s : &str) -> Option<LinkedList<&str>> {
    let mut selector_begin = None;

    let mut escape = false;

    let mut ret = LinkedList::new();

    for (i, c) in s.char_indices() {
        if let Some(b) = selector_begin {
            match c {
                '{' => return None,
                '}' => {
                    ret.push_back(&s[b..i+1]);
                    selector_begin = None;
                }

                _ => (),
            }
        } else {
            if escape {
                escape = false;
            } else {
                match c {
                    '{' => selector_begin = Some(i),
                    '\\' => escape = true,
                    _ => (),
                }
            }
        }
    }

    if let None = selector_begin {
        Some(ret)
    } else {
        None
    }
}

fn parse_selector(s : &str, value : &Value) -> Option<Value> {
    if s.starts_with('{') && s.ends_with('}') {
        let desc = s[1..s.len()-1].trim().split('.');
        let mut val = value;

        debug!("{:?}", desc);

        for i in desc {
            if let Value::Object(m) = &val {
                if let Some(v) = m.get(i) {
                    val = v;
                } else {
                    return None;
                }
            } else {
                return None;
            }
        }

        Some(val.clone())
    } else {
        None
    }
}

fn apply_format(
    format : &str,
    value : &Value,
    selectors : &LinkedList<&str>,
    ret : &mut String,
    sep : &str
) -> tera::Result<()> {
    let mut tmp = String::from(format);

    for i in selectors {
        if let Some(data) = parse_selector(i, value) {
            if let Some(to_replace) = data.as_str() {
                tmp = tmp.replace(i, to_replace);
            } else {
                return Err(tera::Error::from("Data error"));
            }
        } else {
            return Err(tera::Error::from(format!("Error when parsing format: {}", format)))
        }
    }

    if ret.is_empty() {
        ret.push_str(tmp.as_str())
    } else {
        let to_add = String::from(sep) + tmp.as_str();

        ret.push_str(to_add.as_str())
    };

    Ok(())
}

pub struct HighLightManager();

impl Filter for HighLightManager {
    fn filter(&self, value: &Value, args: &HashMap<String, Value>) -> tera::Result<Value> {
        let dat = try_get_value!(
            "highlight",
            "value",
            String,
            value
        );

        let language = optional_arg!(
            String,
            args.get("language"),
            "`highlight`: `language` must be a string naming a programming language"
        ).unwrap_or(String::from("plain"));

        if let Some(highlighted) = markdown::highlight(dat.as_str(), language.as_str()) {
            Ok(Value::String(highlighted))
        } else {
            Err(tera::Error::from(format!("Unknown language `{}`", language)))
        }
    }

    fn is_safe(&self) -> bool {
        true
    }
}

pub fn concat_map(value: &Value, args: &HashMap<String, Value>) -> tera::Result<Value> {
    let map = try_get_value!("concat_map", "value", HashMap<String, Value>, value);

    let format = required_arg!(
            String,
            args.get("format"),
            "`concat_map`: `format` must be a string"
        );

    let sep = optional_arg!(
            String,
            args.get("sep"),
            "`concat_map`: `sep` must be a string"
        ).unwrap_or(String::from(" "));

    let begin = optional_arg!(
            String,
            args.get("begin"),
            "`concat_map`: `begin` must be a string"
        ).unwrap_or(String::from(""));

    let end = optional_arg!(
            String,
            args.get("end"),
            "`concat_map`: `end` must be a string"
        ).unwrap_or(String::from(""));

    if let Some(selectors) = &seek_selector(format.as_str()) {
        let mut ret = String::new();

        for (n, v) in map {
            let val = {
                let mut ret = Map::with_capacity(2);
                ret.insert(String::from("key"), Value::String(n));
                ret.insert(String::from("value"), v);

                Value::Object(ret)
            };
            apply_format(format.as_str(), &val, selectors, &mut ret, sep.as_str())?;
        }

        Ok(Value::String(if ret.is_empty() {
            String::new()
        } else {
            begin + ret.as_str() + end.as_str()
        }))
    } else {
        Err(tera::Error::from("Error when parsing format"))
    }
}

pub fn concat_vec(value: &Value, args: &HashMap<String, Value>) -> tera::Result<Value> {
    let vec = try_get_value!("concat_vec", "value", Vec<Value>, value);

    let format = required_arg!(
        String,
        args.get("format"),
        "`concat_vec`: `format` must be a string"
    );

    let sep = optional_arg!(
        String,
        args.get("sep"),
        "`concat_vec`: `sep` must be a string"
    ).unwrap_or(String::from(" "));

    let begin = optional_arg!(
        String,
        args.get("begin"),
        "`concat_vec`: `begin` must be a string"
    ).unwrap_or(String::from(""));

    let end = optional_arg!(
        String,
        args.get("end"),
        "`concat_vec`: `end` must be a string"
    ).unwrap_or(String::from(""));

    if let Some(selectors) = &seek_selector(format.as_str()) {
        let mut ret = String::new();

        for val in vec {
            apply_format(format.as_str(), &val, selectors, &mut ret, sep.as_str())?;
        }

        Ok(Value::String(if ret.is_empty() {
            String::new()
        } else {
            begin + ret.as_str() + end.as_str()
        }))
    } else {
        Err(tera::Error::from("Error when parsing format"))
    }
}