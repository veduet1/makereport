// TODO: Use Error manager instead of ()

use std::fmt::Formatter;
use std::hash::{Hash, Hasher};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{Error, Visitor};

#[cfg(test)]
mod tests {
    use crate::{CID, CNameSpace, CType};
    use super::is_c_name;

    #[test]
    fn check_names() {
        assert_eq!(true, is_c_name("__yes"));
        assert_eq!(true, is_c_name("dfs8sd"));
        assert_eq!(true, is_c_name("TEST"));
        assert_eq!(true, is_c_name("_dsq_dsfd0sdf0_135240"));

        assert_eq!(false, is_c_name("0sdf"));
    }

    #[test]
    fn check_cid() {
        assert_eq!(Some((0,CNameSpace::Global)), CID::get_info("d45_DSjDE_"));
        assert_eq!(Some((7,CNameSpace::Structs)), CID::get_info("struct d45_DSjDE_"));
        assert_eq!(Some((5,CNameSpace::Enums)), CID::get_info("enum d45_DSjDE_"));
        assert_eq!(Some((6,CNameSpace::Unions)), CID::get_info("union d45_DSjDE_"));

        assert_eq!(None, CID::get_info("error d45_DSjDE_"));
    }

    #[test]
    fn check_type() {
        assert_eq!(Some((0,10,CNameSpace::Global)), CType::get_info("d45_DSjDE_"));
        assert_eq!(Some((0,10,CNameSpace::Global)), CType::get_info("d45_DSjDE_ **"));
        assert_eq!(Some((7,17,CNameSpace::Structs)), CType::get_info("struct d45_DSjDE_ **"));
        assert_eq!(Some((5,15,CNameSpace::Enums)), CType::get_info("enum d45_DSjDE_ **"));
        assert_eq!(Some((6,16,CNameSpace::Unions)), CType::get_info("union d45_DSjDE_ **"));

        assert_eq!(Some((0,4,CNameSpace::Global)), CType::get_info("char [10]"));
        assert_eq!(Some((0,4,CNameSpace::Global)), CType::get_info("char [SDSDF_FSDDSF]"));

        assert_eq!(Some((0,4,CNameSpace::Global)), CType::get_info("char[10]"));
        assert_eq!(Some((0,4,CNameSpace::Global)), CType::get_info("char[SDSDF_FSDDSF]"));

        assert_eq!(None, CType::get_info("***"));
        assert_eq!(None, CType::get_info("9sd"));
        assert_eq!(None, CType::get_info("9sd *"));
        assert_eq!(None, CType::get_info("char [10 00]"));
    }
}

#[inline]
fn check_cname_step(c : char, first : bool) -> bool {
    (c.is_alphanumeric() || c == '_') && !(first && c.is_numeric())
}

#[inline]
fn check_modifier_step(c : char, state : TableParsingState) -> Option<TableParsingState> {
    match state {
        TableParsingState::None => match c {
            '*' => Some(TableParsingState::Pointer),
            '[' => Some(TableParsingState::InTable),
            _   => None,
        },
        TableParsingState::Pointer => if c != '*' { None } else { Some(state) },
        TableParsingState::InTable => {
            if c == ']' { Some(TableParsingState::TableEnd) }
            else if c.is_numeric() { Some(TableParsingState::Number) }
            else if check_cname_step(c, true) { Some(TableParsingState::CName) }
            else { None }
        },
        TableParsingState::TableEnd => None,
        TableParsingState::ValueEnd => if c == ']' { Some(TableParsingState::TableEnd) } else { None },
        TableParsingState::Number => {
            if c == ']' { Some(TableParsingState::TableEnd) }
            else if c.is_numeric() { Some(state) }
            else { None }
        },
        TableParsingState::CName => {
            if c == ']' { Some(TableParsingState::TableEnd) }
            else if check_cname_step(c, false) { Some(state) }
            else { None }
        },
    }
}

fn is_c_name(value : &str) -> bool {
    let mut first = true;

    for c in value.chars() {
        if !check_cname_step(c, first) {
            return false;
        }

        first = false;
    }

    true
}

fn is_c_name_with_modifier(value : &str) -> Option<usize> {
    let mut first = true;
    let mut state = TableParsingState::None;
    let mut cname_len = 0;

    for c in value.chars() {
        if let TableParsingState::None = state {
            if !check_cname_step(c, first) {
                if let Some(s) = check_modifier_step(c, state) {
                    state = s;
                } else {
                    return None;
                }
            } else {
                cname_len += 1;
            }

            first = false;
        } else {
            if let Some(s) = check_modifier_step(c, state) {
                state = s;
            } else {
                return None;
            }
        }
    }

    if cname_len > 0 {
        Some(cname_len)
    } else {
        None
    }
}


struct CNameVisitor;
struct CIDVisitor;
struct CTypeVisitor;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum CNameSpace {
    Global, // Functions, types, Enum values, Defines…
    Structs, // Struct definition
    Enums, // Enum definition
    Unions, // Union definition
}

#[derive(Copy, Clone)]
enum TableParsingState {
    None,
    Pointer,
    TableEnd,
    InTable,
    ValueEnd,
    CName,
    Number,
}

#[derive(Debug, Clone)]
pub struct CName(String);

#[derive(Debug, Clone)]
pub struct CID {
    dat : String,

    base_begin : usize,

    namespace : CNameSpace,
}

#[derive(Debug, Clone)]
pub struct CType {
    dat : String,

    base_begin : usize,
    base_end : usize,

    namespace : CNameSpace,
}



impl CName {
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl CID {
    fn get_info(value : &str) -> Option<(usize, CNameSpace)> {
        let mut split = value.split(" ");

        if let Some(first) = split.next() {
            if let Ok(ns) = CNameSpace::try_from(first) {
                if let Some(second) = split.next() {
                    if is_c_name(second) {
                        while let Some(t) = split.next() {
                            if !t.is_empty() {
                                return None;
                            }
                        }

                        if let Some(pos) = value.find(second) {
                            Some((pos, ns))
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                } else {
                    None
                }
            } else {
                if is_c_name(first) {
                    while let Some(t) = split.next() {
                        if !t.is_empty() {
                            return None
                        }
                    }

                    Some((0, CNameSpace::Global))
                } else {
                    None
                }
            }
        } else {
            None
        }
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        self.dat.as_str()
    }

    #[inline]
    pub fn get_namespace(&self) -> CNameSpace {
        self.namespace
    }

    #[inline]
    pub fn get_c_name(&self) -> &str {
        &self.dat[self.base_begin..]
    }

    pub fn clone_c_name(&self) -> CName {
        CName(self.get_c_name().to_string())
    }
}

impl CType {
    fn get_info(value : &str) -> Option<(usize, usize, CNameSpace)> {
        let mut pos = None;
        let mut name_space = None;
        let mut state = TableParsingState::None;

        for sym in value.split(" ") {
            if !sym.is_empty() {
                if pos.is_none() {
                    if name_space.is_none() {
                        if let Ok(sp) = CNameSpace::try_from(sym) {
                            name_space = Some(sp);
                        } else {
                            name_space = Some(CNameSpace::Global);
                            if let Some(len) = is_c_name_with_modifier(sym) {
                                if let Some(p) = value.find(sym) {
                                    pos = Some((
                                        p,
                                        len
                                    ))
                                } else {
                                    return None;
                                }
                            } else {
                                return None;
                            }
                        }
                    } else {
                        if let Some(len) = is_c_name_with_modifier(sym) {
                            if let Some(p) = value.find(sym) {
                                pos = Some((
                                    p,
                                    len
                                ))
                            } else {
                                return None;
                            }
                        } else {
                            return None;
                        }
                    }
                } else {
                    // Check if this definition is for an array ([###]) or a pointer (*)
                    for c in sym.chars() {
                        if let Some(s) = check_modifier_step(c, state) {
                            state = s;
                        } else {
                            return None
                        }
                    }
                    state = TableParsingState::ValueEnd;
                }
            }
        }

        if let Some((begin, len)) = pos {
            if let Some(sp) = name_space {
                Some((begin, begin + len, sp))
            } else {
                None
            }
        } else {
            None
        }
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        self.dat.as_str()
    }

    #[inline]
    pub fn get_namespace(&self) -> CNameSpace { self.namespace }

    #[inline]
    pub fn get_c_name(&self) -> &str {
        &self.dat[self.base_begin..self.base_end]
    }

    pub fn clone_c_name(&self) -> CName {
        CName(self.get_c_name().to_string())
    }
}



impl TryFrom<String> for CName {
    type Error = ();
    
    fn try_from(value: String) -> Result<Self, Self::Error> {
        if is_c_name(value.as_str()) {
            Ok(Self(value))
        } else {
            Err(())
        }
    }
}

impl TryFrom<&str> for CName {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if is_c_name(value) {
            Ok(Self(String::from(value)))
        } else {
            Err(())
        }
    }
}


impl TryFrom<String> for CID {
    type Error = ();

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if let Some((base_begin, namespace)) = CID::get_info(value.as_str()) {
            Ok(Self {
                dat: value,
                base_begin,
                namespace
            })
        } else {
            Err(())
        }
    }
}

impl TryFrom<&str> for CID {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if let Some((base_begin, namespace)) = CID::get_info(value) {
            Ok(Self {
                dat: String::from(value),
                base_begin,
                namespace
            })
        } else {
            Err(())
        }
    }
}


impl TryFrom<String> for CType {
    type Error = ();

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if let Some((base_begin, base_end, namespace)) = CType::get_info(value.as_str()) {
            Ok(Self {
                dat: value,
                base_begin, base_end,
                namespace
            })
        } else {
            Err(())
        }
    }
}

impl TryFrom<&str> for CType {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if let Some((base_begin, base_end, namespace)) = CType::get_info(value) {
            Ok(Self {
                dat: String::from(value),
                base_begin, base_end,
                namespace
            })
        } else {
            Err(())
        }
    }
}


impl TryFrom<String> for CNameSpace {
    type Error = ();

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.as_str() {
            "struct" => Ok(Self::Structs),
            "enum" => Ok(Self::Enums),
            "union" => Ok(Self::Unions),

            _ => Err(())
        }
    }
}

impl TryFrom<&str> for CNameSpace {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "struct" => Ok(Self::Structs),
            "enum" => Ok(Self::Enums),
            "union" => Ok(Self::Unions),

            _ => Err(())
        }
    }
}



impl From<CName> for CID {
    fn from(v: CName) -> Self {
        let dat = v.0;

        Self {
            base_begin: 0,
            namespace: CNameSpace::Global,
            dat,
        }
    }
}

impl From<CName> for CType {
    fn from(v: CName) -> Self {
        let dat = v.0;

        Self {
            namespace: CNameSpace::Global,
            base_end: dat.len(),
            base_begin: 0,
            dat
        }
    }
}

impl From<CID> for CType {
    fn from(v: CID) -> Self {
        Self {
            namespace: CNameSpace::Global,
            base_end: v.dat.len(),
            base_begin: v.base_begin,
            dat: v.dat
        }
    }
}



impl PartialEq<Self> for CName {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl Hash for CName {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}

impl Eq for CName {}


impl PartialEq<Self> for CID {
    fn eq(&self, other: &Self) -> bool {
        self.dat.eq(&other.dat)
    }
}

impl Hash for CID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.dat.hash(state)
    }
}

impl Eq for CID {}


impl PartialEq<Self> for CType {
    fn eq(&self, other: &Self) -> bool {
        self.dat.eq(&other.dat)
    }
}

impl Hash for CType {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.dat.hash(state)
    }
}

impl Eq for CType {}



impl ToString for CName {
    fn to_string(&self) -> String {
        self.0.clone()
    }
}

impl ToString for CID {
    fn to_string(&self) -> String {
        self.dat.clone()
    }
}

impl ToString for CType {
    fn to_string(&self) -> String {
        self.dat.clone()
    }
}



impl<'de> Visitor<'de> for CNameVisitor {
    type Value = CName;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a string which encodes a C element's name")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where E: Error {
        if let Ok(t) = CName::try_from(v) {
            Ok(t)
        } else {
            Err(E::custom(format!("{} does not encode a C element", v)))
        }
    }
}

impl<'de> Deserialize<'de> for CName {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_str(CNameVisitor)
    }
}


impl<'de> Visitor<'de> for CIDVisitor {
    type Value = CID;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a string which encodes a C element's type declaration")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where E: Error {
        if let Ok(t) = CID::try_from(v) {
            Ok(t)
        } else {
            Err(E::custom(format!("{} does not encode a C type declaration", v)))
        }
    }
}

impl<'de> Deserialize<'de> for CID {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_str(CIDVisitor)
    }
}


impl<'de> Visitor<'de> for CTypeVisitor {
    type Value = CType;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a string which encodes a C type (name *, struct name or enum name)")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where E: Error {
        if let Ok(t) = CType::try_from(v) {
            Ok(t)
        } else {
            Err(E::custom(format!("{} does not encode a C id", v)))
        }
    }
}

impl<'de> Deserialize<'de> for CType {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_str(CTypeVisitor)
    }
}

impl Serialize for CName {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(self.as_str())
    }
}

impl Serialize for CID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(self.as_str())
    }
}

impl Serialize for CType {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        serializer.serialize_str(self.as_str())
    }
}