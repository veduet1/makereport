mod c_symbol;
mod error;
mod rich_element;

use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::path::{Path, PathBuf};
use serde::{Serialize, Deserialize};

pub use c_symbol::{CName, CType, CID, CNameSpace};
pub use error::{Error, Result};
pub use rich_element::RichElement;

pub fn load<P : AsRef<Path>>(path : P) -> Result<ReportFormat> {
    let dat = std::fs::read_to_string(path)?;

    Ok(serde_json::from_str(dat.as_str())?)
}

fn default_lang() -> String {
    String::from("en")
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ReportFormat {
    pub name : String,
    pub version : u8,
    #[serde(default="default_lang")]
    pub lang : String,
    #[serde(default)]
    pub description : RichElement,
    #[serde(default)]
    pub authors : Vec<String>,
    #[serde(default)]
    pub filedir : PathBuf,
    #[serde(default)]
    pub use_makefile : bool,
    pub files : HashMap<PathBuf, ReportFile>,
    pub functions : HashMap<CName, ReportFunction>,
    #[serde(default)]
    pub data_structures : HashMap<String, DataStructure>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ReportFile {
    #[serde(rename = "type")]
    pub file_type : FileType,
    pub description : RichElement,
    #[serde(default)]
    pub dependencies : Vec<PathBuf>,
    #[serde(default)]
    pub functions : Vec<CName>,
    #[serde(default)]
    pub structs : HashMap<CName, Struct>,
    #[serde(default)]
    pub types : HashMap<CName, Variable>,
    #[serde(default)]
    pub symbols : HashMap<CName, Symbol>,
    #[serde(default)]
    pub enums : HashMap<CName, Enum>,
    #[serde(default)]
    pub unions : HashMap<CName, Struct>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ReportFunction {
    pub description : RichElement,
    #[serde(default)]
    pub dependencies_function : Vec<CName>,
    #[serde(default)]
    pub dependencies : Vec<CID>,
    #[serde(default)]
    pub arguments : Vec<PositionnalVariable>,
    #[serde(rename = "return")]
    pub return_val : Option<Variable>,
    #[serde(default)]
    pub local_variables : HashMap<CName, Variable>,
    pub implementation : ImplementationInfo,
    #[serde(default)]
    pub private : bool,
    #[serde(skip_deserializing)]
    pub filedef : PathBuf,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Struct {
    pub description : RichElement,
    #[serde(default)]
    pub dependencies : Vec<CID>,
    pub fields : Vec<PositionnalVariable>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Enum {
    pub description : RichElement,
    #[serde(default)]
    pub dependencies : Vec<CID>,
    pub fields : Vec<PositionnalSymbol>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Variable {
    pub description : RichElement,
    #[serde(rename = "type")]
    pub c_type : CType,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PositionnalVariable {
    pub name : CName,
    pub description : RichElement,
    #[serde(rename = "type")]
    pub c_type : CType,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Symbol {
    pub description : RichElement,
    pub value : String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PositionnalSymbol {
    pub name : CName,
    pub description : RichElement,
    pub value : String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ImplementationInfo {
    pub file : PathBuf,
    pub lines : ImplementationLines,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ImplementationLines {
    pub begin : usize,
    pub end : usize,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct DataStructure {
    pub description : RichElement,
    pub elements : Vec<DataStructureElement>,
    pub functions : HashSet<CName>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub struct DataStructureElement {
    pub file : PathBuf,
    pub name : CID,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum FileType {
    #[serde(rename="Source code")]
    SourceCode,
    Header,
}