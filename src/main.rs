mod gvmaker;
mod tera_module;
mod helper;

use std::collections::LinkedList;
use tera::Context;

use lazy_static::lazy_static;

use std::convert::Infallible;
use std::net::SocketAddr;
use std::ops::Deref;
use std::path::Path;
use std::sync::{Arc, RwLock};
use std::time::Duration;
use hyper::{Body, Request, Response, Server, StatusCode};
use hyper::server::conn::AddrStream;
use hyper::service::{make_service_fn, service_fn};
use notify::{RecursiveMode, Watcher};
use syntect::parsing::SyntaxSet;

use tracing::{error, info, warn, Level};
use tracing_subscriber::FmtSubscriber;

const PUBLIC_PATH : &'static str = "public";
const TMP_PDF : &'static str = "/tmp/generated.pdf";

const LISTEN_IP : [u8; 4] = [127, 0, 0, 1];
const LISTEN_PORT : u16 = 3000;

const MIME_DEFAULT : &'static str = "application/octet-stream";
const MIME_PLAIN   : &'static str = "text/plain";
const MIME_CSS     : &'static str = "text/css";
const MIME_HTML    : &'static str = "text/html";
const MIME_PDF     : &'static str = "application/pdf";

lazy_static!{
    pub static ref HIGHLIGHT_CONTEXT : SyntaxSet = SyntaxSet::load_defaults_newlines();
}

#[inline]
fn make_internal_server_error() -> Response<Body> {
    Response::builder()
        .status(StatusCode::INTERNAL_SERVER_ERROR)
        .header(hyper::header::CONTENT_TYPE, "text/html; charset=\"utf-8\"")
        .body(Body::from(format!("Erreur 500: Le serveur ne parvient pas à vous fournir la ressource demandée, veuillez réessayer ultérieurement")))
        .unwrap()
}

fn watcher_loop(mgr : Arc<RwLock<tera::Tera>>, rx : std::sync::mpsc::Receiver<notify::DebouncedEvent>) {
    loop {
        std::thread::sleep(Duration::from_secs(1));
        if let Ok(_) = rx.recv() {
            if let Ok(mut lock) = mgr.write() {
                match lock.full_reload() {
                    Err(e) => error!("Reload error: {}", e),

                    _ => (),
                }
            }
        }
    }
}

async fn make_page<P : AsRef<Path>>(addr : &SocketAddr, out_path : P) -> std::io::Result<std::process::ExitStatus> {
    let output = tokio::process::Command::new("weasyprint").args([
        format!("http://{}", addr),
        out_path.as_ref().to_string_lossy().to_string()
    ]).output().await?;

    if let Ok(stderr) = String::from_utf8(output.stderr) {
        for line in stderr.split('\n') {
            if !line.is_empty() {
                if let Some((level, info)) = line.split_once(": ") {
                    match level {
                        "WARNING" => warn!(target: "weasyprint", "{}", info),
                        "ERROR" => error!(target: "weasyprint", "{}", info),
                        _ => {},
                    }
                }
            }
        }
    }

    if let Ok(stdout) = String::from_utf8(output.stdout) {
        for line in stdout.split('\n') {
            if !line.is_empty() {
                info!(target: "weasyprint", "{}", line);
            }
        }
    }

    Ok(output.status)
}

async fn handle(mgr : Arc<RwLock<tera::Tera>>, addr : SocketAddr, req: Request<Body>) -> Result<Response<Body>, Infallible> {
    let path = req.uri().path();
    let file_path = format!("{}{}", PUBLIC_PATH, path);
    let begin = std::time::SystemTime::now();

    let ret = Ok(if path == "/" {
        match mgr.read() {
            Ok(locked) => {
                let res = match mkpage(locked.deref()) {
                    Some(v) => Response::new(Body::from(v)),
                    None => make_internal_server_error()
                };

                res
            },
            Err(e) => {
                error!("{}", e);

                make_internal_server_error()
            }
        }
    } else if path == "/page" {
        match make_page(&addr, TMP_PDF).await {
            Ok(v) => {
                if !v.success() {
                    if let Some(code) = v.code() {
                        error!("process exited with a non null code: {}", code);
                    } else {
                        error!("process exited with a non null code");
                    }
                }
                match std::fs::read(TMP_PDF) {
                    Ok(dat) => Response::builder()
                        .header(hyper::header::CONTENT_TYPE, MIME_PDF)
                        .body(Body::from(dat))
                        .unwrap(),
                    Err(e) => {
                        error!("{}", e);

                        make_internal_server_error()
                    }
                }
            }
            Err(e) => {
                error!("{}", e);

                make_internal_server_error()
            }
        }
    } else if let Ok(f) = std::fs::read(file_path.clone()) {
        let mime = if let Some(os_ext) = Path::new(file_path.as_str()).extension() {
            if let Some(ext) = os_ext.to_str() {
                match ext {
                    "pdf" => MIME_PDF,
                    "css" => MIME_CSS,
                    "html" => MIME_HTML,
                    "txt" => MIME_PLAIN,
                    _ => MIME_DEFAULT,
                }
            } else {
                MIME_DEFAULT
            }
        } else {
            MIME_DEFAULT
        };
        Response::builder()
            .header(hyper::header::CONTENT_TYPE, mime)
            .body(Body::from(f)).unwrap()
    } else {
        Response::builder()
            .status(StatusCode::NOT_FOUND)
            .header(hyper::header::CONTENT_TYPE, "text/html; charset=\"utf-8\"")
            .body(Body::from(format!("Erreur 404: La ressource `{}` n’a pas été trouvée", path)))
            .unwrap()
    });

    if let Ok(dur) = std::time::SystemTime::now().duration_since(begin) {
        info!("{} generated in {}ms", path, dur.as_millis());
    }

    ret
}

fn generate_style() {
    let theme = syntect::highlighting::ThemeSet::get_theme("templates/themes/theme.tmTheme").unwrap();

    std::fs::write("public/css/highlight.css", syntect::html::css_for_theme_with_class_style(
        &theme,
        syntect::html::ClassStyle::Spaced
    )).unwrap();
}

#[tokio::main]
async fn main() {
    // a builder for `FmtSubscriber`.
    let subscriber = FmtSubscriber::builder()
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to stdout.
        .with_max_level(Level::INFO)
        // completes the builder.
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .expect("setting default subscriber failed");

    let (tx, rx) = std::sync::mpsc::channel();

    let addr = SocketAddr::from((LISTEN_IP, LISTEN_PORT));

    info!("Loading template");

    generate_style();

    let mgr = Arc::new(RwLock::new(tera::Tera::new("templates/**/*.html").unwrap()));
    let wrmgr = mgr.clone();

    match mgr.write() {
        Ok(mut mgr) => {
            mgr.register_filter("highlight", tera_module::HighLightManager());
            mgr.register_filter("concat_map", tera_module::concat_map);
            mgr.register_filter("concat_vec", tera_module::concat_vec);
        },
        Err(e) => {
            error!("{}", e);
            return;
        }
    }

    let mut watcher = notify::watcher(tx, std::time::Duration::from_secs(1)).unwrap();
    watcher.watch("templates", RecursiveMode::Recursive).unwrap();

    info!("Generating complete");

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(move |_conn : &AddrStream| {
        let addr = addr.clone();
        let mgr = mgr.clone();

        let service = service_fn(move |req| {
            handle(mgr.clone(), addr, req)
        });

        async move { Ok::<_, Infallible>(service) }
    });

    // Then bind and serve...
    let server = Server::bind(&addr).serve(make_service);
    info!("Listening on: http://{}", addr);

    let _ = std::thread::spawn(move || {
        watcher_loop(wrmgr, rx);
    });

    // And run forever...
    if let Err(e) = server.await {
        error!("{}", e);
    }
}

fn mkpage(mgr : &tera::Tera) -> Option<String> {
    let mut desc = match report_format::load("/home/venceslas/git/tp1-sdd/info.json") {
        Ok(d) => d,
        Err(e) => {
            error!("{}", e);
            return None
        },
    };

    // Génération des éléments intermédiaires
    let mut structs = LinkedList::new();
    let mut enums = LinkedList::new();
    let mut unions = LinkedList::new();
    let mut types = LinkedList::new();
    let mut symbols = LinkedList::new();

    for (filename, file) in &desc.files {
        for (name, dat) in &file.structs {
            structs.push_back((name.to_string(), dat.clone(), filename));
        }
        for (name, dat) in &file.enums {
            enums.push_back((name.to_string(), dat.clone(), filename));
        }
        for (name, dat) in &file.unions {
            unions.push_back((name.to_string(), dat.clone(), filename));
        }
        for (name, dat) in &file.types {
            types.push_back((name.to_string(), dat.clone(), filename));
        }
        for (name, dat) in &file.symbols {
            symbols.push_back((name.to_string(), dat.clone(), filename));
        }
        for name in &file.functions {
            if let Some(fd) = desc.functions.get_mut(name) {
                fd.filedef = filename.clone();
            } else {
                error!("Function {} declared in {} but not used", name.as_str(), filename.to_str().unwrap());
            }
        }
    }

    let mut ctx = match Context::from_serialize(&desc) {
        Ok(ctx) => ctx,
        Err(e) => {
            error!("{}", e);
            return None
        },
    };

    ctx.insert("structs", &structs);
    ctx.insert("enums", &enums);
    ctx.insert("unions", &unions);
    ctx.insert("types", &types);
    ctx.insert("symbols", &symbols);

    match mgr.render("main.html", &ctx) {
        Ok(dat) => Some(dat),
        Err(e) => {
            if let Some(src) = std::error::Error::source(&e) {
                error!("{}: {}", e, src);
            } else {
                error!("{}", e);
            }
            return None
        },
    }
}
