use std::borrow::Cow;
use std::fmt::Formatter;
use markdown::{Block, ListItem, OrderedListType, Span};
use markdown::Block::*;
use markdown::Span::*;
use serde::{Deserializer, Serializer};
use serde::de::Error;

pub fn gen_spans(elements : &Vec<Span>) -> String {
    let mut ret = String::new();

    for span in elements {
        let generated = match span {
            Break => String::from("<br>"),
            Text(text) => text.clone(),
            Code(code) => format!("<code>{}</code>", code),
            Literal(literal) => literal.to_string(),
            Link(data, link, tip_opt) => if let Some(tip) = tip_opt {
                format!("<a href=\"{}\" title=\"{}\">{}</a>", link, tip, gen_spans(data))
            } else {
                format!("<a href=\"{}\">{}</a>", link, gen_spans(data))
            }
            RefLink(data, link, _) => format!("<a href=\"#{}\">{}</a>", link, gen_spans(data)),
            Image(data, src, tip_opt) => if let Some(tip) = tip_opt {
                format!("<img src=\"{}\" title=\"{}\" alt=\"{}\">", src, tip, data)
            } else {
                format!("<img src=\"{}\" alt=\"{}\">", src, data)
            }
            Emphasis(data) => format!("<em>{}</em>", gen_spans(data)),
            Strong(data) => format!("<strong>{}</strong>", gen_spans(data)),
        };
        ret.push_str(generated.as_str())
    }

    ret
}

fn gen_code_block<'a>(code : &'a str, language : Option<&str>) -> Cow<'a, str> {
    if let Some(lang) = language {
        if let Some(out) = markdown::highlight(code, lang) {
            Cow::Owned(out)
        } else {
            Cow::Borrowed(code)
        }
    } else {
        Cow::Borrowed(code)
    }
}

fn gen_list(items : &[ListItem], list_type : Option<OrderedListType>) -> String {
    let (mut ret, end) = if let Some(list_type) = list_type {
        (format!("<ul class=\"{}\">", match list_type {
            OrderedListType::Lowercase => "lowercase",
            OrderedListType::Uppercase => "uppercase",
            OrderedListType::LowercaseRoman => "lowercase-roman",
            OrderedListType::UppercaseRoman => "uppercase-roman",
            OrderedListType::Numeric => "numeric"
        }),
         "</ol>")
    } else {
        (String::from("<ul>"), "</ul>")
    };

    for item in items {
        let generated = format!("<li>{}</li>", match item {
            ListItem::Simple(spans) => gen_spans(spans),
            ListItem::Paragraph(blocks) => gen_html(blocks),
        });
        ret.push_str(generated.as_str())
    }

    ret.push_str(end);

    ret
}

pub fn gen_html(elements : &Vec<Block>) -> String {
    let mut ret = String::new();
    for block in elements {
        let generated = match block {
            Header(data, level) => if *level == 0 {
                format!("<div class=\"main title\" role=\"heading\">{}</div>", gen_spans(data))
            } else if *level <= 6 {
                format!("<h{level}>{data}</h{level}>", level=level, data=gen_spans(data))
            } else {
                format!("<div class=\"title level-{level}\" role=\"heading\">{data}</div>", level=level, data=gen_spans(data))
            },
            Paragraph(data) => format!("<p>{}</p>", gen_spans(data)),
            Blockquote(data) => format!("<blockquote>{}</blockquote>", gen_html(data)),
            CodeBlock(language, code) => format!("<pre><code>{}</code></pre>", gen_code_block(code.as_str(), if let Some(lang) = language {
                Some(lang.as_str())
            } else {
                None
            })),
            SectionBlock(level, spans) => if let Some(level) = level {
                format!("<div class=\"{}\">{}</div>", level, gen_spans(spans))
            } else {
                format!("<div>{}</div>", gen_spans(spans))
            },
            LinkReference(text, link, _) => format!("<a href=\"{}\">{}</a>", link, text),
            OrderedList(list, list_type) => gen_list(list, Some(list_type.clone())),
            UnorderedList(list) => gen_list(list, None),
            Raw(raw) => raw.clone(),
            Hr => String::from("<hr>"),
        };
        ret.push_str(generated.as_str());
    }
    ret
}

#[derive(Default, Debug, Clone)]
pub struct RichElement(Vec<Block>);

struct RichElementVisitor;

impl RichElement {
    #[inline]
    pub fn as_markdown(&self) -> String {
        markdown::generate_markdown(&self.0)
    }

    #[inline]
    pub fn as_html(&self) -> String {
        gen_html(&self.0)
    }

    #[inline]
    pub fn from_markdown(dat : &str) -> Self {
        Self(markdown::tokenize(dat))
    }
}

impl<'de> serde::de::Visitor<'de> for RichElementVisitor {
    type Value = RichElement;
    
    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a string encoding markdown")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> where E: Error {
        Ok(RichElement::from_markdown(v))
    }
}

impl serde::Serialize for RichElement {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let generated = self.as_html();
        serializer.serialize_str(generated.as_str())
    }
}

impl<'de> serde::Deserialize<'de> for RichElement {
    fn deserialize<D>(deserializer: D) -> Result<RichElement, D::Error>
        where
            D: Deserializer<'de>,
    {
        deserializer.deserialize_str(RichElementVisitor)
    }
}
