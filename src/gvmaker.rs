use std::collections::{HashMap, LinkedList};
use tokio::io::AsyncWriteExt;
use tracing::error;
use report_format::{CName, CNameSpace, Struct};

#[cfg(test)]
mod Tests {
    use std::collections::LinkedList;
    use crate::gvmaker::concat_list;

    #[test]
    fn concat() {
        let mut list = LinkedList::new();
        list.push_back("A");
        list.push_back("B");
        list.push_back("C");

        assert_eq!(concat_list(&list, " | "), "<A> A | <B> B | <C> C");
    }
}

#[derive(Copy, Clone, Debug)]
#[allow(unused)]
pub enum Engine {
    DOT,
    Neato,
    Twopi,
    Circo,
    FDP,
    Osage,
    PatchWork,
    SFDP,
}

impl Engine {
    fn command_name(&self) -> &'static str {
        use Engine::*;

        match self {
            DOT => "dot",
            Neato => "neato",
            Twopi => "twopi",
            Circo => "circo",
            FDP => "fdp",
            Osage => "osage",
            PatchWork => "patchwork",
            SFDP => "sfdp",
        }
    }
}

fn concat_list<S : AsRef<str>>(list : &LinkedList<S>, sep : &str) -> String {
    let mut len = 0;
    let mut fields = LinkedList::new();

    for i in list {
        let s = i.as_ref();
        if len != 0 { len += sep.len(); }
        len += 2*s.len() + 3;

        fields.push_back(s);
    }

    let mut ret = String::with_capacity(len);
    let mut first = true;

    for i in fields {
        if !first {
            ret.push_str(sep);
        }
        ret.push('<');
        ret.push_str(i);
        ret.push('>');
        ret.push(' ');
        ret.push_str(i);
        first = false;
    }

    ret
}

fn lines_to_string<S : AsRef<str>>(lines : &LinkedList<S>) -> String {
    let mut len = 0;

    for i in lines {
        len += i.as_ref().len() + 1;
    }

    let mut v = String::with_capacity(len);

    for i in lines {
        v.push_str(i.as_ref());
        v.push('\n');
    }

    v.shrink_to_fit();

    v
}

pub async fn render(engine : Engine, input : &str) -> std::io::Result<String> {
    let mut child = tokio::process::Command::new(engine.command_name())
        .arg("-Tsvg")
        .stdin(std::process::Stdio::piped())
        .stdout(std::process::Stdio::piped())
        .spawn()?;

    if let Some(ref mut in_buf) = child.stdin {
        in_buf.write_all(input.as_bytes()).await?;
    }

    let out_buf : std::process::Output = child.wait_with_output().await?;

    if let Ok(err) = std::str::from_utf8(out_buf.stderr.as_slice()) {
        for i in err.split('\n') {
            if !i.is_empty() {
                error!(target: "graphviz", "{}", i);
            }
        }
    }

    if out_buf.status.success() {
        match std::str::from_utf8(out_buf.stdout.as_slice()) {
            Ok(out) => Ok(String::from(out)),
            Err(e) => {
                error!("Bad output format: {}", e);

                Err(std::io::Error::from(std::io::ErrorKind::Other))
            }
        }
    } else {
        if let Some(exit_code) = out_buf.status.code() {
            error!("Process exited with non-nul exit code: {}", exit_code);
        } else {
            error!("Process exited with error");
        }

        Err(std::io::Error::from(std::io::ErrorKind::InvalidInput))
    }
}

pub async fn generate_data_structure_desc(
    data_structure : HashMap<CName, Struct>
) -> std::io::Result<String> {
    let mut lines = LinkedList::new();
    let mut links = LinkedList::new();

    lines.push_back(String::from("digraph d {"));

    // Rendu des structures de données
    for (n, s) in &data_structure {
        let mut fields = LinkedList::new();

        for i in &s.fields {
            fields.push_back(i.name.as_str());

            if let CNameSpace::Structs = i.c_type.get_namespace() {
                let link_with = i.c_type.clone_c_name();

                if data_structure.contains_key(&link_with) {
                    links.push_back((
                        i.name.as_str(),
                        i.c_type.as_str(),
                        link_with
                    ))
                }
            }
        }

        lines.push_back(format!("\"{}\" [label=\"{}\" shape=\"record\"];",
            n.as_str(),
            concat_list(&fields, " | ")
        ));
    }

    // Rendu des liens entre elles
    for (from_s, from_f, to_s) in links {
        lines.push_back(format!("\"{}\":{} -> \"{}\";",
            from_s,
            from_f,
            to_s.as_str()
        ));
    }

    let dat = lines_to_string(&lines);

    render(Engine::DOT, dat.as_str()).await
}